# unity3d ci example

[![pipeline status](https://gitlab.com/gableroux/unity3d-gitlab-ci-example/badges/master/pipeline.svg)](https://gitlab.com/gableroux/unity3d-gitlab-ci-example/commits/master)
[![Build Status](https://travis-ci.com/GabLeRoux/unity3d-ci-example.svg?branch=master)](https://travis-ci.com/GabLeRoux/unity3d-ci-example)

(Not affiliated with Unity Technologies)

## Template YAML
```yaml
stages:
  - versioning
  - prepare
  - build_and_test
  - deploy

include:
- project: ci/unity3d-gitlab-ci-example
  ref: v1.1.1
  file: 
  - /.gitlab-ci/fastlane.yml
  - /.gitlab-ci/builds/android.yml
  - /.gitlab-ci/builds/ios.yml
  - /.gitlab-ci/templates/get-unity-version.gitlab-ci.yml

variables:
  BUILD_NAME: <ExampleBuildName>
  BUNDLE_NAME: <com.corp.example>
  GIT_DEPTH: "1"
  GIT_STRATEGY: clone
  GITLAB_ACCESS_TOKEN: $CI_REPO_READ_TOKEN
  GITLAB_URL: gitlab.spacerhino.games
  IMAGE: unityci/editor
  IMAGE_VERSION: "1.0.1"
  RELEASE_STATUS: draft
  UNITY_ACTIVATION_FILE: ./unity3d.alf
  UNITY_CI_VERSION: v1.1.1
  UNITY_DIR: $CI_PROJECT_DIR
  VERSION_NUMBER_VAR: prototype
  VERSION_BUILD_VAR: $CI_PIPELINE_IID

image: $IMAGE:$UNITY_VERSION-base-$IMAGE_VERSION

.develop_rules: 
  rules: &develop_rules
  - if: $CI_COMMIT_BRANCH =~ /^ci/
  - if: $CI_COMMIT_BRANCH =~ /^feature/
  - if: $CI_COMMIT_BRANCH == "develop"
  - if: $CI_COMMIT_BRANCH == "prototype"

.deployment_rules:
  rules: &deployment_rules
  - if: $CI_COMMIT_BRANCH == "main"
  - if: $CI_COMMIT_BRANCH == "master" 

get-unity-version:
  tags: ["docker"]

build-android:
  extends: .build-gpu-android
  rules: *develop_rules
  
build-android-bundle:
  extends: .build-gpu-android-bundle
  rules: *deployment_rules

build-ios-xcode:
  extends: .build-gpu-ios-xcode

deploy-android-diawi:
  extends: .fastlane-android
  rules: *develop_rules
  variables: 
    LANE: internal
  tags: ["docker"]

deploy-ios-diawi:
  extends: .fastlane-ios
  variables: 
    LANE: internal

deploy-ios-develop:
  extends: .fastlane-ios
  variables: 
    LANE: develop
  rules:
  - when: manual

deploy-android-playstore:
  extends: .fastlane-android
  rules: *deployment_rules
  tags: ["docker"]
  
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always
```

## Documentation

**Visit [game.ci/docs/gitlab/getting-started](https://game.ci/docs/gitlab/getting-started).**

## Shameless plug

I made this for free as a gift to the video game community so if this tool helped you and you would like to support me, send your love to [Totema Studio](https://totemastudio.com) on Patreon: :beers:

[![Totema Studio Logo](./doc/totema-studio-logo-217.png)](https://patreon.com/totemastudio)

[![Become a Patron](./doc/become_a_patron_button.png)](https://www.patreon.com/bePatron?c=1073078)

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)

