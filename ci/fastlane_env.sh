#!/usr/bin/env bash

set -ex

target_dir="${1:-.}"

if [ ! -d "${target_dir}" ]; then
    echo "${target_dir} does not exist"
    exit
fi

echo -n "\
APPLE_ID=${APPLE_ID}
MATCH_REPO_URL=${MATCH_REPO_URL}
MATCH_REPO_USER=${CI_REGISTRY_USER}
MATCH_REPO_TOKEN=${CI_JOB_TOKEN}
BUNDLE_NAME=${BUNDLE_NAME}\
" > "${target_dir%/}/.env"