#!/usr/bin/env bash

set -e
set -x

echo "Building for $BUILD_TARGET"

export BUILD_PATH=$UNITY_DIR/Builds/$BUILD_TARGET/
mkdir -p $BUILD_PATH

NO_GRAPHICS=(-nographics)

if [[ "${*}" == *"--enable-rendering"* ]]
then
  NO_GRAPHICS=()
  if [ -z "${NVIDIA_VISIBLE_DEVICES}" ]
  then
      echo "No GPU device detected, using llvmpipe renderer"
  else
    echo "NVIDIA GPU device detected"
    nvidia-smi
    # See https://github.com/game-ci/unity-test-runner/pull/113#issuecomment-824818548
    # as to why unity-editor can't run using X virtual framebuffer
    sed -i "s/xvfb-run -ae \/dev\/stdout //g" /usr/bin/unity-editor
    UNITY_EXECUTABLE="unity-editor"
  fi
fi

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' unity-editor} \
  -projectPath $UNITY_DIR \
  -quit \
  -batchmode \
  "${NO_GRAPHICS[@]}" \
  -buildTarget $BUILD_TARGET \
  -customBuildTarget $BUILD_TARGET \
  -customBuildName $BUILD_NAME \
  -customBuildPath $BUILD_PATH \
  -executeMethod BuildCommand.PerformBuild \
  -logFile /dev/stdout

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

ls -la $BUILD_PATH
[ -n "$(ls -A $BUILD_PATH)" ] # fail job if build folder is empty